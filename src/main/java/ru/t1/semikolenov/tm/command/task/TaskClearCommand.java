package ru.t1.semikolenov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-clear";

    @NotNull
    public static final String DESCRIPTION = "Remove all tasks.";

    @Nullable
    public static final String ARGUMENT = null;

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @Nullable
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[TASK CLEAR]");
        @NotNull final String userId = getUserId();
        getTaskService().clear(userId);
    }

}
