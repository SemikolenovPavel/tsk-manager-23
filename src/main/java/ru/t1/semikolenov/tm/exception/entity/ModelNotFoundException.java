package ru.t1.semikolenov.tm.exception.entity;

public final class ModelNotFoundException extends  AbstractEntityNotFoundException {

    public ModelNotFoundException() {
        super("Error! Model not found...");
    }

}
