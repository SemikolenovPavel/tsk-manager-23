package ru.t1.semikolenov.tm.exception.field;

public final class EmptyIdException extends AbstractFieldException {

    public EmptyIdException() {
        super("Error! Id is empty...");
    }

}
