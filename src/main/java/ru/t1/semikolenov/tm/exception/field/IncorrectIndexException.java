package ru.t1.semikolenov.tm.exception.field;

public final class IncorrectIndexException extends AbstractFieldException {

    public IncorrectIndexException() {
        super("Error! Index is incorrect...");
    }

}
