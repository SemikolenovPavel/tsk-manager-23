package ru.t1.semikolenov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.api.repository.ITaskRepository;
import ru.t1.semikolenov.tm.api.service.ITaskService;
import ru.t1.semikolenov.tm.enumerated.Status;
import ru.t1.semikolenov.tm.exception.entity.TaskNotFoundException;
import ru.t1.semikolenov.tm.exception.field.*;
import ru.t1.semikolenov.tm.model.Task;

import java.util.*;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull final ITaskRepository repository) {
        super(repository);
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        if (projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    public Task create(
            @NotNull final String userId,
            @NotNull final String name) {
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final Task task = repository.create(userId, name);
        return task;
    }

    @NotNull
    @Override
    public Task create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull String description
    ) {
        if (name.isEmpty()) throw new EmptyNameException();
        if (description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Task task = repository.create(userId, name, description);
        return task;
    }

    @NotNull
    @Override
    public Task create(
            @NotNull final String userId,
            @NotNull String name,
            @NotNull String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    ) {
        @NotNull final Task task = repository.create(name, description);
        if (dateBegin == null) throw new IncorrectDateBeginException();
        else task.setDateBegin(dateBegin);
        if (dateEnd == null) throw new IncorrectDateEndException();
        else task.setDateEnd(dateEnd);
        task.setUserId(userId);
        return task;
    }

    @NotNull
    @Override
    public Task updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final Task task = Optional.ofNullable(repository.findOneById(userId, id))
                .orElseThrow(TaskNotFoundException::new);
        if (name.isEmpty()) throw new EmptyNameException();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return task;
    }

    @NotNull
    @Override
    public Task updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description,
            @Nullable Date dateBegin, Date dateEnd
    ) {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final Task task = Optional.ofNullable(repository.findOneById(userId, id))
                .orElseThrow(TaskNotFoundException::new);
        if (name.isEmpty()) throw new EmptyNameException();
        task.setName(name);
        task.setDescription(description);
        if (dateBegin == null) throw new IncorrectDateBeginException();
        else task.setDateBegin(dateBegin);
        if (dateEnd == null) throw new IncorrectDateEndException();
        else task.setDateEnd(dateEnd);
        task.setUserId(userId);
        return task;
    }

    @NotNull
    @Override
    public Task updateByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final String name,
            @NotNull final String description
    ) {
        if (index < 0) throw new IncorrectIndexException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final Task task = Optional.ofNullable(repository.findOneByIndex(userId, index))
                .orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return task;
    }

    @NotNull
    @Override
    public Task updateByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final String name,
            @NotNull final String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    ) {
        if (index < 0) throw new IncorrectIndexException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final Task task = Optional.ofNullable(repository.findOneByIndex(userId, index))
                .orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        if (dateBegin == null) throw new IncorrectDateBeginException();
        else task.setDateBegin(dateBegin);
        if (dateEnd == null) throw new IncorrectDateEndException();
        else task.setDateEnd(dateEnd);
        task.setUserId(userId);
        return task;
    }

    @NotNull
    @Override
    public Task changeStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status) {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final Task task = Optional.ofNullable(repository.findOneById(userId, id))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(status);
        task.setUserId(userId);
        return task;
    }

    @NotNull
    @Override
    public Task changeStatusByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final Status status
    ) {
        if (index < 0) throw new IncorrectIndexException();
        @NotNull final Task task = Optional.ofNullable(repository.findOneByIndex(userId, index))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(status);
        task.setUserId(userId);
        return task;
    }

}
